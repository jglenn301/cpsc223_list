#include <stdio.h>

#include "list.h"

void print_list(list *l);

int main(int argc, char **argv)
{
  list *nums = list_create();
  if (nums == NULL)
    {
      return 1;
    }

  for (int i = 0; i < 10; i++)
    {
      list_add_end(nums, i * i);
      print_list(nums);
      printf("\n");
    }

  print_list(nums);
  printf("\n");

  printf("INSERT 99 at index 2\n");
  list_add_at(nums, 99, 2);
  print_list(nums);
  printf("\n");

  printf("SET index 3 to -1\n");
  int removed = list_set(nums, -1, 3);
  print_list(nums);
  printf(", removed %d\n", removed);

  printf("REMOVE from index 5\n");
  removed = list_remove_at(nums, 5);
  print_list(nums);
  printf(", removed %d\n", removed);

  for (int i = 0; i < 10; i++)
    {
      bool found = list_contains(nums, i);
      size_t index = list_find(nums, i);
      printf("found %d: %d", i, found);
      if (index != LIST_NOWHERE)
	{
	  printf(" at %lu\n", index);
	}
      else
	{
	  printf(" NOWHERE\n");
	}
    }

  printf("REMOVING\n");
  while (list_size(nums) > 0)
    {
      list_remove_at(nums, 0);
      print_list(nums);
      printf("\n");
    }
  
  list_destroy(nums);
}

void print_list(list *l)
{
  printf("[");
  size_t size = list_size(l);
  for (size_t i = 0; i < size; i++)
    {
      if (i > 0)
	{
	  printf(", ");
	}
      printf("%d", list_get(l, i));
    }
  printf("]");
}

