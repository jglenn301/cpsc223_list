CC=gcc
CFLAGS=-Wall -pedantic -std=c99 -g3

ArrayList: list_test.o array_list.o
	${CC} ${CCFLAGS} -o $@ $^ -lm

array_list.o: list.h
list_test.o: list.h
