#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include "list.h"

struct _list
{
  element *elements;  // pointer to the array containing the elements
  size_t size;        // the number of elements actually in the list
  size_t capacity;    // the max elements the current array can hold
};

#define LIST_INITIAL_CAPACITY (2)
#define LIST_MINIMUM_CAPACITY (2)

list *list_create()
{
  // make space for the meta-data struct
  list *result = malloc(sizeof(list));

  // make an array for future elements
  result->elements = malloc(sizeof(element) * LIST_INITIAL_CAPACITY);

  // initialize size and capacity
  result->size = 0;
  result->capacity = result->elements != NULL ? LIST_INITIAL_CAPACITY : 0;

  return result;
}

size_t list_size(const list *l)
{
      return l->size;
}

element list_get(list *l, size_t i)
{
  return l->elements[i];
}

element list_set(list *l, element e, size_t i)
{
  element old;
  old = l->elements[i];

  l->elements[i] = e;

  return old;
}

bool list_add_end(list *l, element to_add)
{
  return list_add_at(l, to_add, l->size);
}

bool list_add_at(list *l, element to_add, size_t i)
{
  // make sure there's enough space
  if (l->capacity > 0 && l->size == l->capacity)
    {
      element *bigger = realloc(l->elements, sizeof(element) * l->capacity * 2);
      if (bigger != NULL)
	{
	  l->elements = bigger;
	  l->capacity *= 2;
	}
    }
  
  if (l->size < l->capacity)
    {
      // copy elements to higher index to make room
      for (size_t copy_to = l->size; copy_to > i; copy_to--)
	{
	  l->elements[copy_to] = l->elements[copy_to - 1];
	}
      l->elements[i] = to_add;
      l->size++;
      return true;
    }
  else
    {
      return false;
    }
}

element list_remove_at(list *l, size_t i)
{
  // remember old element to return later
  element old = l->elements[i];
  
  // copy elements back to fill the hole
  for (size_t copy_to = i; copy_to < l->size - 1; copy_to++)
    {
      l->elements[copy_to] = l->elements[copy_to + 1];
    }

  l->size--;

  // resize if too much capacity
  if (l->size > l->capacity / 4 && l->capacity > LIST_MINIMUM_CAPACITY)
    {
      size_t new_cap = l->size > LIST_MINIMUM_CAPACITY ? l->size : LIST_MINIMUM_CAPACITY;
      element *smaller = realloc(l->elements, sizeof(element) * new_cap);
      if (smaller != NULL)
	{
	  // shouldn't fail, but the standard doesn't guarantee it won't
	  l->elements = smaller;
	  l->capacity = new_cap;
	}
    }
      
  return old;
}

bool list_contains(const list *l, element e)
{
  return (list_find(l, e) != LIST_NOWHERE);
}

size_t list_find(const list *l, element e)
{
  // sequential search for the element
  size_t i = 0;
  while (i < l->size && l->elements[i] != e)
    {
      i++;
    }

  // determine why we stopped
  if (i == l->size)
    {
      // not found
      return LIST_NOWHERE;
    }
  else
    {
      // found at index i
      return i;
    }
}

void list_destroy(list *l)
{
  // free the array
  free(l->elements);

  // free the meta-data struct
  free(l);
}
